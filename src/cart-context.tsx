import { useEffect, useState } from "react";
import { createContext } from "react";
import { Product } from "./entities";
import { setCookie, destroyCookie, parseCookies } from "nookies";



interface CartState {
    products:Product[],
    addProduct : (value: Product) => void,
    removeProduct : (value: Product) => void,
    clear: () => void
    
}

export const CartContext = createContext({} as CartState);


export const CartContextProvider = ({children}:any) => {
    const [products, setProducts] = useState<Product[]>([]);

    useEffect (() => {
        const { cart } = parseCookies(null, "cart");
        if (cart) {
            setProducts(JSON.parse(cart));
        }
    },[])


    function addProduct(value:Product) {
        setProducts([...products, value])
        setCookie(null, 'cart', JSON.stringify(products)) 
    }

    function removeProduct(value:Product) {
        setProducts(products.filter((product) => product.id !== value.id));
        setCookie(null, 'cart', JSON.stringify(products))
    }

    function clear() {
        setProducts([])
        setCookie(null, 'cart', JSON.stringify(products)) 
    }

    return (
        <CartContext.Provider value={{products, addProduct, removeProduct, clear}}>
            {children}
        </CartContext.Provider>
    )

}