import axios from "axios";
import { Order } from "./entities";

export async function postOrder(order:Order){
    const response = await axios.post<Order>('/api/order', order);
    return response.data;
}

export async function GetOrder(){
    const response = await axios.get<Order[]>('/api/history');
    return response.data;
}