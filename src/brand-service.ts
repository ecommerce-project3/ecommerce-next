import axios from "axios";
import { Brand } from "./entities";






export async function fetchAllBrands(){
    const response = await axios.get<Brand[]>('/api/brand/');
    return response.data;
}