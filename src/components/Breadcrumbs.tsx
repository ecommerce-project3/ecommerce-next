import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const categoryLabels: {[key: string]: string} = {
  '1': 'Hauts',
  '2': 'Bas',
  '3': 'Accessoires',
  '4': 'Chaussures',
  '5': 'Vestes'
};

const Breadcrumb = () => {
  const router = useRouter();
  const path = router.asPath.split('/').filter(Boolean);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    router.push('/');
  };

  return (
    <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        {path.map((item, index) => {
          const href = '/' + path.slice(0, index + 1).join('/');
          const isLast = index === path.length - 1;
          const label = categoryLabels[item] || item; // utilise l'étiquette de catégorie si elle existe dans l'objet, sinon utilise la valeur numérique

          return (
            <li key={index} className={`breadcrumb-item ${isLast ? 'active' : ''}`}>
              {isLast ? (
                label
              ) : (
                <Link href={href} passHref>
                  <button className='btnAriane' onClick={handleClick}>{label}</button>
                </Link>
              )}
            </li>
          );
        })}
      </ol>
    </nav>
  );
};

export default Breadcrumb;