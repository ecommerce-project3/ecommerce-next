import { AuthContext } from "@/auth/auth-context";
import { fetchUser, updateUser } from "@/auth/auth-service";
import { User , Order} from "@/entities"
import { GetOrder } from "@/order-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react"

interface Props {
    from:string
}

export default function AccountList ({from} : Props) {
    const [fromPage, setFromPage] = useState(from);

    const [user, setUser] = useState<User>({
        firstName: '',
        lastName: '',
        email: '',
        address: '',
        phone: '',
        password: ''
      });

      const [orders, setOrders] = useState<Order[]>([])

      const router = useRouter();
      const { token, setToken } = useContext(AuthContext);
    
      function logout() {
        setToken(null)
        if (fromPage == "account") {

            router.push('/')
        }
      }
    
      useEffect(() => {
        fetchUser().then(data => {
          setUser(data);
        }).catch(error => {
          if (error.response.status == 401) {
            router.push('/login')
          }
        });
      }, [])

      useEffect(() => {
        GetOrder()
          .then(data => {
            setOrders(data);
          })
          .catch(error => {
            console.error(error);
          });
      }, [user]);
    
    
      function handleChange(event: any) {
        setUser({
          ...user,
          [event.target.name]: event.target.value
        });
      }
    
    
      async function fetchUpdateUser(event: any) {
    
        event?.preventDefault()
        const updated = await updateUser(user);
        setUser(updated);
        setShowEdit(!showEdit)
      }
    
      const [showEdit, setShowEdit] = useState(false);
    
      const [showHistory, setShowHistory] = useState(false);

      return (
        <>
          <div className="container-fluid">
            <div className="row justify-content-center">
              
              <div className="col-md-8">
                <ul className="list-group">
                  <li className="list-group-item">Nom : {user?.lastName}</li>
                  <li className="list-group-item">Prénom : {user?.firstName}</li>
                  <li className="list-group-item">Email : {user?.email}</li>
                  <li className="list-group-item">Télephone : {user?.phone}</li>
                  <li className="list-group-item">Adresse : {user?.address}</li>
                  {fromPage != "cart" && orders.length > 0 &&
          <li className="list-group-item">
            <Link href="" onClick={() => setShowHistory(!showHistory)}>{!showHistory ? "Afficher l'historique des commandes" : "Masquer l'historique des commandes"}</Link>
            {showHistory && orders.map(item =>
              <>
            <p className="fw-bold">Numéro de commande : {item.id}</p>
            <p>Statut : {item.status}</p>
            <p>Date de livraison : {item.estimateDate}</p>
            <p>Produits : {item.product.map(prod => 
              <>
              <p>{prod.name} : {prod.price}€ - Taille : {prod.size}</p>
              </>)}</p>
            </>
              )}
          </li>
          }
                </ul>
              </div>
            </div>
    
    
          </div>
          {fromPage != "cart" &&
          <div className="row justify-content-center">
            <button className="btn btn-danger logout-btn mt-2" onClick={logout}>Se déconnecter</button>
          </div>
          }
        
    
          <div className="row justify-content-center">
            <button className="btn btn-success logout-btn mt-2" onClick={() => setShowEdit(!showEdit)}>Modifier</button>
          </div>
    
         

          {showEdit && <>
            <div className="row justify-content-center">
              <form onSubmit={fetchUpdateUser} className="col-md-6 m-4 p-3" id="form">
    
                <div className="mb-3">
                  <label htmlFor="name" className="form-label ">Nom</label>
                  <input type="text" name='lastName' value={user.lastName} onChange={handleChange} className="form-control" id="firstName" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="lastName" className="form-label ">Prénom</label>
                  <input type="text" name='firstName' value={user.firstName} onChange={handleChange} className="form-control" id="lastName" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="email" className="form-label ">Email</label>
                  <input type="text" name='email' value={user.email} onChange={handleChange} className="form-control" id="email" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="address" className="form-label ">Adresse</label>
                  <input type="text" name='address' value={user.address} onChange={handleChange} className="form-control" id="address" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="phone" className="form-label ">N° de téléphone</label>
                  <input type="text" name='phone' value={user.phone} onChange={handleChange} className="form-control" id="phone" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="password" className="form-label ">Mot de passe</label>
                  <input type="password" name='password' value={user.password} onChange={handleChange} className="form-control" id="password" required />
                </div>
                <div className="row justify-content-center">
    
                  <button type="submit" className="btn btn-success col-3" >Valider</button>
                </div>
    
              </form>
            </div>

            

          </>}
            
          </>
        
      )
}