
import { Product } from '@/entities';
import { WishListContext } from '@/wishList-context';
import React, { useContext, useState } from 'react';
import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';

interface Props {
  product:Product;
}

export default function WishListButton({product}:Props) {
  const [showPopover, setShowPopover] = useState(false);

  const handleButtonClick = () => {
    setShowPopover(true);
    setTimeout(() => {
      setShowPopover(false);
    }, 1500);
    addWishList()
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Ajouté à mes préférés</Popover.Header>
      <Popover.Body>Votre article est dans votre wishList</Popover.Body>
    </Popover>
  );

  const {products, addWish} = useContext(WishListContext);

  function addWishList () {
    addWish(product)
    console.log(products)
}

  return (
    <>

      <OverlayTrigger
        trigger="click"
        placement="top"
        overlay={popover}
        show={showPopover}
      >
        <Button variant="success rounded-circle m-2" onClick={handleButtonClick}>
        <i className="bi bi-suit-heart"></i>
        </Button>
      </OverlayTrigger>
    </>
  );
}
// function addWishList() {
//     throw new Error('Function not implemented.');
// }

