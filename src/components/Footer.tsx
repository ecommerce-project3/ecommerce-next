import Link from "next/link";


export default function Footer() {

    return (
        <>
            <div className="row footer mt-5 p-5 text-center">
                <div className="col-4">
                    <Link href="#">Mentions légales</Link>
                </div>
                <div className="col-4">
                    <Link href="#">Politique de confidentialité</Link>
                </div>
                <div className="col-4">
                    <Link href="#">A propos de nous</Link>
                </div>
            </div>
        </>
    )
}