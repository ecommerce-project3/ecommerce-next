import { AuthContext } from "@/auth/auth-context";
import Products from "@/pages/products";
import jwtDecode from "jwt-decode";
import Link from "next/link";
import router from "next/router";
import { useContext } from "react";

export default function Header() {
  const {token} = useContext(AuthContext);

  function isAdmin() {
  if (token) {
    const decoded = jwtDecode<any>(token);
    if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
    }
    return false
  }
  }

  const handleSearch = (event:any) => {
    event.preventDefault();
    const searchTerm = event.target.elements.search.value;
    router.push('/search/'+searchTerm);
    event.target.elements.search.value = null;
  };

  return (
    <>
      <header className="container-fluid">
        <Link className="main-h1" href="/"><h1>ASSSOS</h1></Link>
      

        <nav className="row justify-content-center">

           <div className="row justify-content-center">
              <div className="col-8 col-sm-6 col-md-3 mt-3 mb-3">
              <form className="d-flex" role="search"  onSubmit={handleSearch}>
                  <input className="form-control me-2" type="search" placeholder="Rechercher" aria-label="Search"  name="search" />
                  <button className="btn btn-success search-btn" type="submit"><i className="bi bi-search"></i></button>
                </form>
              </div>
              </div>

              <div className="row justify-content-center">
                <ul className="nav-ul col-12 col-sm-6 col-md-4">
                  
                  <li className="nav-item">
                    <Link className="nav-link" href={"/category/"+1}>Hauts</Link>
                  </li>
                    <span className="_1hUkP8H" aria-hidden="true">
                      |
                    </span>
                  <li className="nav-item">
                    <Link className="nav-link" href={"/category/"+2}>Bas</Link>
                  </li>
                  <span className="_1hUkP8H" aria-hidden="true">
                      |
                    </span>
                  <li className="nav-item">
                    <Link className="nav-link" href={"/category/"+5}>Vestes</Link>
                  </li>
                  <span className="_1hUkP8H" aria-hidden="true">
                      |
                    </span>
                  <li className="nav-item">
                    <Link className="nav-link" href={"/category/"+4}>Chaussures</Link>
                  </li>
                  <span className="_1hUkP8H" aria-hidden="true">
                      |
                    </span>
                  <li className="nav-item">
                    <Link className="nav-link" href={"/category/"+3}>Accessoires</Link>
                  </li>


                  <li className="nav-item">
                    <Link className="nav-link" href={token ? "/account" : "/login"}><i className="bi bi-person"></i></Link>
                  </li>


                  <li className="nav-item">
                    <Link className="nav-link" href="/cart"><i className="bi bi-cart"></i></Link>
                  </li>


                  <li className="nav-item">
                    <Link className="nav-link" href="/wishList"><i className="bi bi-suit-heart"></i></Link>
                  </li>

                  {isAdmin() && 
                  <li className="nav-item">
                    <Link className="nav-link" href="/add"><i className="bi bi-plus-circle"></i></Link>
                  </li>}
                </ul>

                </div>

        </nav>

      </header>

      {/* <nav className="navbar navbar-expand-lg bg-body-tertiary">
    <div className="container-fluid flex-column">
        <div className="d-flex"> 
  <a className="navbar-brand" href="#">Friperie</a>
    </div>
<div>
  <form className="d-flex" role="search">
        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
        <button className="btn btn-outline-success" type="submit">Search</button>
      </form>
</div>
    <div className="row">
        <div className="col-12">
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse d-flex justify-content-center align-items-center" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <a className="nav-link" aria-current="page" href="#">Hauts</a>
        </li>
        <li className="nav-item vertical-line">
          <a className="nav-link" href="#">Bas</a>
        </li>
    
        <li className="nav-item vertical-line">
          <a className="nav-link" href="#">Vestes</a>
        </li>
        <li className="nav-item vertical-line">
          <a className="nav-link" href="#">Accessoires</a>
        </li>
      </ul>
      </div> 
      </div>
    </div>
</div>
</nav> */}

    </>
  )
}