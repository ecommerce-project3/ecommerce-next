

export default function MenuSlide() {

    return (
        <>
            <div id="carouselExampleInterval" className="carousel slide" data-bs-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-item active" data-bs-interval="2000">
                        <img src="/img/shoppez-nos-nouveautes.png" className="d-block w-100" alt="..."/>
                        <div className="bouton position-absolute bottom-0 start-50 translate-middle-x">
                       <a className="nav-ul slider-btn bg-white  p-2 mb-2" href="/category/4">Shoppez les chaussures </a>
                        </div>
                    </div>
                    <div className="carousel-item" data-bs-interval="2000">
                        <img src="/img/vetements.png" className="d-block w-100" alt="..."/>
                        <div className="bouton position-absolute bottom-0 start-50 translate-middle-x">
                        <a className="nav-ul slider-btn bg-white  p-2 mb-2" href="/category/1">Shoppez les hauts </a>
                        </div>
                    </div>
                    <div className="carousel-item" data-bs-interval="2000">
                        <img src="/img/vetements-2.png" className="d-block w-100" alt="..."/>
                        <div className="bouton position-absolute bottom-0 start-50 translate-middle-x">
                        <a className="nav-ul slider-btn bg-white  p-2 mb-2" href="/category/5">Shoppez les vestes </a>
                        </div>
                    </div>
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>

        </>
    )
}