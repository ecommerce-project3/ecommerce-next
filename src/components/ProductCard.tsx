import { Product } from "@/entities"
import Link from "next/link";
import CartButton from "./CartButton";
import WishListButton from "./WishListButton";

interface Props {
    product: Product;
}

export default function ProductCard({ product }: Props) {
    return (
        <div className="card col-11 col-md-5 col-lg-3 m-2" >

            <Link className="title-link" href={"/product/" + product.id}><img src={product.pictures[0].image} className="img-fluid card-img-top" alt={product.pictures[0].alt} /></Link>
            <div className="card-body">
                <div className="title-heart">
                    <Link className="title-link" href={"/product/" + product.id}><h5 className="card-title">{product.name}</h5></Link>
                    {/* <i className="bi bi-suit-heart"></i> */}
                    <WishListButton product={product}/>
                </div>
                <p className="card-text priceCart">{product.price}€ <CartButton product={product}/></p>

            </div>
        </div>
    )
}