import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useState } from "react";

interface Props {
    from:string
}

export default function LoginForm ({from}:Props) {
    const router = useRouter();
    const {token, setToken} = useContext(AuthContext);
    const [error, setError] = useState('');
    const [log, setLog] = useState({
        email:'',
        password:''
    })

    const [fromPage, setFromPage] = useState(from);
    

    function handleChange(event:any) {
        setLog({
            ...log,
            [event.target.name] : event.target.value
        })
    }

    async function handleSubmit (event:any){
        event.preventDefault();
        setError('');
        try {
            setToken(await login(log.email, log.password));
            if (fromPage == "login") {

                router.push('/account');
            }
        }catch(error:any) {
            console.log(error)
            if(error.response?.status == 401) {
                setError('Identifiant/mot de passe invalide');
            } else {
                setError('Server error');
            }
        }
        

    }

    return (
        <>
        <div className="container-fluid">
        <div className="row justify-content-center mt-4">
        {token ?  

        <button className="btn btn-danger logout-btn" onClick={() => setToken(null)}>Se déconnecter</button>
            : 
                   
            
                <div className="col-md-6">
                     {error && <p className="error-msg">{error}</p>}
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" onChange={handleChange} required/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputPassword1" className="form-label">Mot de passe</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" name="password" onChange={handleChange} required/>
                        </div>
        
                        <button type="submit" className="btn btn-primary">Se connecter</button>
                    </form>
                    <button className="btn btn-success mt-2"><Link href="/signup" className="sign-btn">S'inscrire</Link></button>
                </div>
        
        }
    
         </div>
        </div>
        </>
    )
}