
import { CartContext } from '@/cart-context';
import { Product } from '@/entities';
import React, { useContext, useState } from 'react';
import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';

interface Props {
  product:Product;
}

export default function CartButton({product}:Props) {
  const [showPopover, setShowPopover] = useState(false);

  const handleButtonClick = () => {
    setShowPopover(true);
    setTimeout(() => {
      setShowPopover(false);
    }, 1500);
    addCart()
  };

  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Ajouté au panier</Popover.Header>
      <Popover.Body>Votre article est dans votre panier</Popover.Body>
    </Popover>
  );

  const {products, addProduct} = useContext(CartContext);

  function addCart () {
    addProduct(product)
    console.log(products)
}



  return (
    <>

      <OverlayTrigger
        trigger="click"
        placement="top"
        overlay={popover}
        show={showPopover}
      >
        <Button variant="success" onClick={handleButtonClick}>
          Ajouter au panier
        </Button>
      </OverlayTrigger>
    </>
  );
}
