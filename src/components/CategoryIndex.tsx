import Link from "next/link";


export default function CategoryIndex() {
    return (
        <div className="row justify-content-center">

            <div className="col-md-10">

                <div className="row justify-content-center">

                    <div className="col-sm-8 col-md-4 col-lg-3 mt-5">
                        <Link href='#' className="titre">
                            <h5>HAUTS</h5>
                            <img src="/img/Rectangle 27.jpg" alt="" className="img-fluid" />
                        </Link>
                    </div>

                    <div className="col-sm-8 col-md-4 col-lg-3 mt-5">
                        <Link href='#' className="titre">
                            <h5>BAS</h5>
                            <img src="/img/Rectangle 28.png" alt="" className="img-fluid" />
                        </Link>
                    </div>

                    <div className="col-sm-8 col-md-4 col-lg-3 mt-5">
                        <Link href='#' className="titre">
                            <h5>CHAUSSURES</h5>
                            <img src="/img/chaussures.png" alt="" className="img-fluid" />
                        </Link>
                    </div>

                </div>

                <div className="row justify-content-center">

                    <div className="col-sm-8 col-md-4 col-lg-3 mt-5">
                        <Link href='#' className="titre">
                            <h5>VESTES</h5>
                            <img src="/img/veste.png" alt="" className="img-fluid" />
                        </Link>
                    </div>

                    <div className="col-sm-8 col-md-4 col-lg-3 mt-5" >
                        <Link href='#' className="titre">
                            <h5>ACCESSOIRES</h5>
                            <img src="/img/accessoire.png" alt="" className="img-fluid" />
                        </Link>
                    </div>

                    <div className="col-sm-8 col-md-4 col-lg-3 mt-5">
                        <Link href='#' className="titre">
                            <h5>TOUT</h5>
                            <img src="/img/tout.png" alt="" className="img-fluid" />
                        </Link>
                    </div>

                </div>

            </div>

        </div>
    )
}