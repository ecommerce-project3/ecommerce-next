import Link from "next/link";
import { useState } from "react";


export default function MenuCard() {

    const categoryName = [
        {name:"HAUTS", image:'/img/Rectangle 27.jpg', route:"/category/"+1},
        {name:"BAS", image:'/img/Rectangle 28.png', route:"/category/"+2},
        {name:"CHAUSSURES", image:'/img/chaussures.png', route:"/category/"+4},
        {name:"VESTES", image:'/img/veste.png', route: "/category/"+5}, 
        {name:"ACCESSOIRES", image:'/img/accessoire.png', route: "/category/"+3},
        {name:"TOUT", image:'/img/tout.png', route: "/products"}
    ]
    return (
        <>
            <div className="row justify-content-center">

                <div className="col-10">

                    <div className="row justify-content-center">
                        {categoryName.map(item => 
                            
                        <div key={item.name} className="col-sm-7 col-md-4 col-lg-3 mt-5 m-2">
                            <Link href={item.route}className="titre">
                                <h5>{item.name}</h5>
                                <img src={item.image} alt={item.name} className="img-fluid" />
                            </Link>
                        </div>
                        
                            )}
                    </div>
                </div>
            </div>
        </>
    )
}