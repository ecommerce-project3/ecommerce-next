import axios from "axios";
import { Category, Product } from "./entities";
import Products from "./pages/products";

export async function fetchAllProducts() {
    const response = await axios.get<Product[]>('/api/product');
    return response.data;
}

export async function fetchOneProduct(id:number){
    const response = await axios.get<Product>('/api/product/'+id);
    return response.data;
}

export async function postProduct(product:Product){
    product.price= Number (product.price)
    const response = await axios.post<Product>('/api/product', product)
    return response.data;
}

export async function fetchByCategory(id:number){
    const response = await axios.get<Product[]>('/api/product/category/'+id);
    return response.data;
}

export async function fetchByCategoryName(id:number){
    const response = await axios.get<Category>('/api/category/'+id);
    return response.data;
}

export async function fetchSearch(search:string){
    const response = await axios.get<Product[]>('/api/product/search/'+search);
    return response.data;
}

export async function deleteProduct(product:Product) {
    const response = await axios.delete('api/product/'+product.id);
    return response.data
}
