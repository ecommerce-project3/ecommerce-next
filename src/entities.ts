export interface Product {
    id?:number,
    category?:string, 
    brand?:string,
    name:string,
    status:string,
    price:number,
    description:string
    size?:string
    pictures:Picture[];
    available?:boolean
}

export interface Picture {
    id?:number,
    image:string,
    alt:string
}


export interface Category {
    id?:number,
    label:string,
    size_type:string
    }


export interface Brand {
    id?:number,
    name?:string
}

export interface User {

    id?: number,
    firstName?: string,
    lastName?: string,
    email: string,
    address?: string,
    phone?: string,
    password: string,
    orders?: []
}

export interface Order {
    id?:number,
    user_id?: number,
    estimateDate?:  string,    
    deliveryMode:string,
    status?: string,
    product:Product[]
}