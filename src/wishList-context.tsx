import { useEffect, useState } from "react";
import { createContext } from "react";
import { Product } from "./entities";
import { setCookie, destroyCookie, parseCookies } from "nookies";



interface WishListState {
    products:Product[],
    addWish : (value: Product) => void,
    removeProduct : (value: Product) => void,
    
}

export const WishListContext = createContext({} as WishListState);


export const WishListContextProvider = ({children}:any) => {
    const [products, setProducts] = useState<Product[]>([]);
    const { wishList } = parseCookies(null, "wishList");

    function addWish(value:Product) {
        setProducts([...products, value])
        setCookie(null, 'wishList', JSON.stringify(products)) 
    }

    function removeProduct(value:Product) {
        setProducts(products.filter((product) => product.id !== value.id));
        setCookie(null, 'wishList', JSON.stringify(products))

    }

    return (
        <WishListContext.Provider value={{products, addWish, removeProduct}}>
            {children}
        </WishListContext.Provider>
    )

}