import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import Header from '@/components/Header'
import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { useEffect } from "react"
import { AuthContextProvider } from "@/auth/auth-context";

import "../auth/axios-config"

import Footer from "@/components/Footer";
import Breadcrumb from "@/components/Breadcrumbs";
import { CartContextProvider } from "@/cart-context";
import { WishListContextProvider } from "@/wishList-context";


export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
    <>
      <AuthContextProvider>
        <CartContextProvider>
          <WishListContextProvider>
          <Header />
          <Breadcrumb />
          <main>
          <Component {...pageProps} />

          </main>
          <Footer />
          </WishListContextProvider>
        </CartContextProvider>
      </AuthContextProvider>
    </>
  )

}
