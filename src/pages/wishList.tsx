
import { useContext } from "react"
import { parseCookies } from "nookies";
import { Product } from "@/entities";
import { WishListContext } from "@/wishList-context";




export default function WishList() {
            
    const { addWish, removeProduct } = useContext(WishListContext);
    const { wishList } = parseCookies(null, "wishList");
    const products = eval(wishList);
    console.log(products)


    return (
        <>
            <div className="container-fluid d-flex justify-content-center ">


                <div className="container-fluid">
                    <h2>Ma wishList</h2>
                    <div className="row d-flex justify-content-center mt-5">

                        <div className="border rounded bg-white col-sm-4 col-md-6">
                            {products?.map((item: Product) =>
                                <>
                                    <h5 key={item.id}>{item.name}</h5>
                                    <p>{item.price} €</p>
                                    <p>Taille : {item.size}</p>
                                    <div className="choice">
                                        <div className="delete">
                                            <button>supprimer</button>
                                        </div>
                                        <div className="wishList">
                                            <button>Déplacer vers mon panier</button>
                                        </div>
                                    </div>
                                </>


                            )}

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}