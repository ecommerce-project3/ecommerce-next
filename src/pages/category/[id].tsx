import ProductCard from "@/components/ProductCard";
import { Category, Product } from "@/entities";
import { fetchByCategory, fetchByCategoryName } from "@/product-service";
import { GetServerSideProps } from "next";
import { useMemo, useState } from "react";

interface Props {
    products: Product[],
    category: Category,
}

export default function CategoryPage({ products, category }: Props) {

    const [formValue, setFormValue] = useState('default')
    const defaultProducts = products.filter(product => product.available);
    
    const renderedProducts = useMemo(() => {
        if (formValue === "desc") {
            return [...products].sort((a: any, b: any) => b.price - a.price);

        } else if (formValue === "asc") {
            return [...products].sort((a: any, b: any) => a.price - b.price);
        } else {
            console.log(defaultProducts);
            return defaultProducts;
            
        }
    }, [formValue, products]);

    function sortBy(e: any) {
        setFormValue(e.target.value);
    }

    return (
        <>
            <div className="container-fluid">
                <h1>{category.label}</h1>
                {/*faire composant mais j'arrive pas a gerer le renderedproducts puis le mettre dans la page tous les prooduits ausis */}
                <div className="row filters">
                    <form className="col-6 col-md-2">
                        <label htmlFor="price" className="mt-2">Trier par :</label>
                        <select className="form-select" aria-label="price" id="price" onChange={sortBy}>
                            <option value="default">Par défaut</option>
                            <option value="asc">Prix croissant</option>
                            <option value="desc">Prix décroissant</option>
                        </select>
                    </form>
                </div>

                <div className="row justify-content-center">
                    {renderedProducts.map((item) =>
                        <ProductCard product={item} key={item.id} />
                    )}
                </div>
            </div>

        </>

    )
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const { id } = context.query;

    try {

        return {
            props: {
                products: await fetchByCategory(Number(id)),
                category: await fetchByCategoryName(Number(id))
            }
        }

    } catch (error) {
        console.log(error);
        return {
            notFound: true
        }
    }

}
