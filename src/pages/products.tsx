import ProductCard from "@/components/ProductCard"
import { Product } from "@/entities"
import { fetchAllProducts } from "@/product-service"
import { GetServerSideProps } from "next"

interface Props {
    products: Product[]
}

export default function Products({ products }: Props) {


    return (
        <>
            <div className="container-fluid">

                <h1>Tous les produits</h1>
                <div className="row justify-content-center">

                    {products.map((item) =>
                        <ProductCard product={item} key={item.id}/>
                    )}

                </div>
            </div>
         
        </>
    )
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    return {
        props: {
            products: await fetchAllProducts()
        }
    }
}