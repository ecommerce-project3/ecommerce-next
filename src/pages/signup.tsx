import { postUser } from "@/auth/auth-service";
import { User } from "@/entities"
import { useRouter } from "next/router";
import { FormEvent, useState } from "react"



export default function signup() {
  const [errors, setErrors] = useState('');
  const router = useRouter();
  const [user, setUser] = useState<User>(
    {
      firstName: '',
      lastName: '',
      email: '',
      address: '',
      phone: '',
      password: ''

    });


  function handleChange(event: any) {
    setUser({
      ...user,
      [event.target.name]: event.target.value
    });
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault();

    try {
      postUser(user);
      router.push('/login');

    } catch (error: any) {
      if (error.response.status == 400) {
        setErrors(error.response.data.detail);
      }

    }
  }

  


  return (
    <>
      <h2 className="mt-2">S'inscrire</h2>
      <div className="row justify-content-center">
        <form onSubmit={handleSubmit} className="col-md-6 m-4 p-3" id="form">

          <div className="mb-3">
            <label htmlFor="name" className="form-label text-white">Nom</label>
            <input type="text" name='firstName' value={user.firstName} onChange={handleChange} className="form-control" id="firstName" required />
          </div>
          <div className="mb-3">
            <label htmlFor="lastName" className="form-label text-white">Prénom</label>
            <input type="text" name='lastName' value={user.lastName} onChange={handleChange} className="form-control" id="lastName" required />
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label text-white">Email</label>
            <input type="text" name='email' value={user.email} onChange={handleChange} className="form-control" id="email" required />
          </div>
          <div className="mb-3">
            <label htmlFor="address" className="form-label text-white">Adresse</label>
            <input type="text" name='address' value={user.address} onChange={handleChange} className="form-control" id="address" required />
          </div>
          <div className="mb-3">
            <label htmlFor="phone" className="form-label text-white">N° de téléphone</label>
            <input type="text" name='phone' value={user.phone} onChange={handleChange} className="form-control" id="phone" required />
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="form-label text-white">Mot de passe</label>
            <input type="password" name='password' value={user.password} onChange={handleChange} className="form-control" id="password" required />
          </div>
          <button type="submit" className="btn btn-success m-2 col-3 btn-lg">S'inscrire</button>

        </form>
      </div>
   </>
  )

}
