import { AuthContext } from "@/auth/auth-context";
import CartButton from "@/components/CartButton";
import WishListButton from "@/components/WishListButton";
import { Product } from "@/entities";
import { deleteProduct, fetchOneProduct } from "@/product-service";
import jwtDecode from "jwt-decode";
import { GetServerSideProps } from "next";
import { Router } from "next/router";
import { useContext, useState } from "react";

interface Props {
    product: Product
}

export default function OneProduct({ product }: Props) {

    //faire un state qui contient l'index de la picture principale

    return (
        <div className="container-fluid">
            <div className="row justify-content-center">
                <div className="productDiv col-10 col-lg-6">
                    <div className="row">
                        <div className="col-lg-7">
                          {/* modifier le css et la structure html pour faire que si l'index principale correspond à l'index de cette picture, alors on lui met la classe "main-picture", sinon on met la classe "secondary-picture" */}
                          {/* sur toutes les images faire que onClick ça mette l'index de l'image cliqué en picture principale */}
                        <img className="img-fluid  mb-4 main-pic" src={product.pictures[0].image} alt={product.pictures[0].alt} />
                        <div className="small-pics">
                            {product.pictures[1] &&
                                <img src={product.pictures[1].image} alt={product.pictures[1].alt} className="col-5"  />}
                            {product.pictures[2] &&   
                                <img src={product.pictures[2].image} alt={product.pictures[2].alt} className="col-5"  />}
                            </div>
                        </div>
                        <div className="col-lg-5">
                            
                            <h5>{product.name}</h5>
                            <p>{product.price}€</p>
                            <p>{product.description}</p>
                            <p>Taille : {product.size} </p>
                            <p>Condition : {product.status}</p>
                            {!product.available && <p>CE PRODUIT N'EST PLUS EN STOCK</p>}
                            {product.available &&
                            <>
                            <CartButton product={product}/>
                            <WishListButton product={product}/>
                            </>
                            }


                        </div>
                    </div>

                </div>
            </div>

        </div>
    )
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const { id } = context.query;

    try {

        return {
            props: {
                product: await fetchOneProduct(Number(id)),
            }
        }

    } catch {
        return {
            notFound: true
        }
    }

}
