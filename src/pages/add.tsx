import { fetchAllBrands } from "@/brand-service";
import { fetchAllCategories } from "@/category-service";
import { Brand, Category, Picture, Product } from "@/entities"
import { postProduct } from "@/product-service";
import { useRouter } from "next/router";
import { FormEvent, useEffect, useState } from "react"



export default function Add () {
    const [category, setCategory] = useState<Category[]>([])
    const [brand, setBrand] = useState<Brand[]>([])
    const router = useRouter();
    const [product, setProduct] = useState<Product>({
        
        name: '',
        status: '',
        price: 0,
        description: '',
        size: '',
        pictures: []
    });

    const [picture, setPicture]= useState<Picture>({
        image: '',
        alt:''
    })

    useEffect(()=>{
        fetchAllCategories().then(data=>{setCategory(data)});
        fetchAllBrands().then(data=>{setBrand(data)});
             
    }, []);

    function handleChange(event: any) {
        setProduct({
            ...product,
            [event.target.name]: event.target.value
        });
    } 

    function handleChangePicture(event: any) {
        setPicture({
            ...picture,
            [event.target.name]: event.target.value
        });
    } 

    async function AddProduct(event:FormEvent) { 
        event.preventDefault();       
        const add = await postProduct({
            ...product,pictures:[picture]});
        router.push('/product/' + add.id)
    }
 
    return (
        <>
            <h2 className="mt-2">Ajouter</h2>
            <div className="row justify-content-center">
                <form onSubmit={AddProduct} className="col-md-6 m-4 p-3" id="form">
                    <div className="mb-3">
                    <label htmlFor="category" className="form-label text-white">Categorie
                         </label>
      
            <select name='category' onChange={handleChange}>
                <option hidden disabled selected value={undefined}> -- select an option -- </option>

                {category.map(item=>
                <option key={item.id} value={item.id}>{item.label}</option>
                )} </select> 
            </div>
                    <div className="mb-3">
                        <label htmlFor="brandId" className="form-label text-white">Brand</label>
                        <select name='brand' onChange={handleChange}>{brand.map(item=>
                <option key={item.id} value={item.id}>{item.name}</option>
                )} </select> 
                    </div>
                    <div className="mb-3">
                        <label htmlFor="name" className="form-label text-white">Name</label>
                        <input type="text" name='name' value={product.name} onChange={handleChange} className="form-control" id="name" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="status" className="form-label text-white">Status</label>
                        <input type="text" name='status' value={product.status} onChange={handleChange} className="form-control" id="status" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="price" className="form-label text-white">Price</label>
                        <input type="number" name='price' value={product.price} onChange={handleChange} className="form-control" id="price" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="description" className="form-label text-white">Description</label>
                        <input type="text" name='description' value={product.description} onChange={handleChange} className="form-control" id="description" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="size" className="form-label text-white">Taille</label>
                        <input type="text" name='size' value={product.size} onChange={handleChange} className="form-control" id="size" required />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="picture" className="form-label text-white">Image 1</label>
                        <input type="text" name='image' value={picture.image} onChange={handleChangePicture} className="form-control" id="picture" required />
                    </div>
                    <button type="submit" className="btn btn-sugccess m-2 col-3 btn-lg">Valider</button>

                </form>
                    </div>
            </>
            )
            }


