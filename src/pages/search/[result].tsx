import ProductCard from "@/components/ProductCard";
import { Product } from "@/entities";
import { fetchSearch } from "@/product-service";
import { GetServerSideProps } from "next";
import { useMemo, useState } from "react";

interface Props {
    products: Product[],
}

export default function ResultPage({ products}: Props) {

    const [formValue, setFormValue] = useState('default')
    const defaultProducts = products;
    
    const renderedProducts = useMemo(() => {
        if (formValue === "desc") {
            return [...products].sort((a: any, b: any) => b.price - a.price);

        } else if (formValue === "asc") {
            return [...products].sort((a: any, b: any) => a.price - b.price);
        } else {
            console.log(defaultProducts);
            return defaultProducts;
            
        }
    }, [formValue, products]);

    function sortBy(e: any) {
        setFormValue(e.target.value);
    }

    

    return (
        <>
            <div className="container-fluid">
                {/* <h1>{result}</h1> */}
                {renderedProducts.length > 0 &&
                <div className="row filters">
                    <form className="col-6 col-md-2">
                        <label htmlFor="price" className="mt-2">Trier par :</label>
                        <select className="form-select" aria-label="price" id="price" onChange={sortBy}>
                            <option value="default">Par défaut</option>
                            <option value="asc">Prix croissant</option>
                            <option value="desc">Prix décroissant</option>
                        </select>
                    </form>
                </div> }

                <div className="row justify-content-center">

                {renderedProducts.length > 0 ? (
                    renderedProducts.map((item) => (
            <ProductCard product={item} key={item.id} />
        ))
    ) : (
        <p>Désolé, aucun produit n'a été trouvé.</p>
    )}
                 
                </div>
            </div>

        </>

    )
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const { result } = context.query;
    

    // try {
        return {
            props: {
                products: await fetchSearch(String(result)),
            }
        }
    // }
    //  catch (error) {
    //     console.log(error);
    //     return {
    //         notFound: true
    //     }
    // }

}
