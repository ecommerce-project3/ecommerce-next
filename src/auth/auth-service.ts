import { User } from "@/entities";
import axios from "axios";

export async function login(email:string,password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, password});
    console.log('from login')
    return response.data.token;
}

export async function postUser(user:User) {
    const response = await axios.post('/api/user' , user);
    return response.data;
}

export async function fetchUser() {
    const repsonse = await axios.get<User>('/api/account');
    return repsonse.data;
}

export async function updateUser(user:User) {
    const response = await axios.patch<User>('http://localhost:8000/api/user', user);
    return response.data;
}

