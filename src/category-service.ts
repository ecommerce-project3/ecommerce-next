import axios from "axios";
import { Category } from "./entities";






export async function fetchByCategoryName(id:number){
    const response = await axios.get<Category>('/api/category/'+id);
    return response.data;
}

export async function fetchAllCategories(){
    const response = await axios.get<Category[]>('/api/category/');
    return response.data;
}